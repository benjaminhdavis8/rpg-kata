= OVERVIEW =
RPG kata is a project based on a lab assigned to students in an introductory
programming class at Brigham Young University. It was designed as an
introduction to polymorphism and included driver code to run and output
information from student test case implementations.

This repo configures the instructions for the lab in the main branch, and it's
primary purpose it to provide a baseline for practicing polymorphism in various
languages. The form <language>/<branch_name> is used to separate a branch
containing an implementation from the specification details.

= DETAILS =
== FIGHTERS ==
Fighters have a set of common attributes, and functions.
The attributes:
- Name - The fighter's name
- Hit Points
  - The amount of health the fighter has, with a specified maximum;
    reaching 0 is equivalent to losing. A Fighter's starting Hit Points (which
    is a Fighter's maximum Hit Points) must be greater than 0. There is no
    upper bound to what a Fighter's starting/maximum Hit Points could be.
- Strength
  - Physical power, used to determine hit point regeneration. Must be greater
    than 0. No maximum value.
- Speed
  - Dexterity and physical movement, used to reduce damage when being attacked.
    Must be greater than 0. No maximum value.
- Magic
  - Magical prowess, used for some special abilities. Must be greater than
    0. No maximum value.

== SPECIALIZATIONS ==
The lab simulates combat between warriors of different classes, namely
- Robot,
- Archer, and
- Cleric.

The three fighter types have unique abilities:
- Robot
  - Relies on strength to deal damage.  Also can use stored energy to
    temporarily increase damage (max/starting energy equal to 2*magic).  Only
    Robots have energy.
- Archer
  - Relies on speed to deal damage.  Also can increase its speed for the
    remainder of the battle (no max bonus speed).
- Cleric
  - Relies on magic to deal damage.  Also can heal itself using mana, restoring
    hit points (max/starting mana equal to 5*magic).  Only Clerics have mana.
 
== FIGHTER INTERFACE ==
The functions:
- getName
  - No inputs.
  - Returns the name of this fighter.
- getMaximumHP
  - No inputs.
  - Returns the maximum hit points of this fighter.
- getCurrentHP
  - No inputs.
  - Returns the current hit points of this fighter.
- getStrength
  - No inputs.
  - Returns the strength stat of this fighter.
- getSpeed
  - No inputs.
  - Returns the speed stat of this fighter.
- getMagic
  - No inputs.
  - Returns the magic stat of this fighter.
- getDamage
  - No inputs.
  - Returns the amount of damage a fighter will deal, which depends on it's
    class.
    Robot:
    This value is equal to the Robot's strength plus any additional damage added for having just used its special ability.
    Archer:
    This value is equal to the Archer's speed.
    Cleric:
    This value is equal to the Cleric's magic.
- takeDamage
  - Given an integer value for damage dealt.
  - Reduces the fighter's current hit points by an amount equal to the given
    damage minus one fourth of the fighter's speed.  This method must reduce
    the fighter's current hit points by at least one.  It is acceptable for
    this method to give the fighter negative current hit points.
                                                                             
    Examples:
    	damage=10, speed=7		=> damage_taken=9
    	damage=10, speed=9		=> damage_taken=8
    	damage=10, speed=50		=> damage_taken=1
- reset
  - No inputs.
  - Restores a fighter's current hit points to its maximum hit points.
                                                                                              
    Robot:
    Also restores a Robot's current energy to its maximum value (which is 2 times its magic).
    Also resets a Robot's bonus damage (calculated when a Robot uses its ability) to 0.
                                                                                              
    Archer:
    Also resets an Archer's current speed to its original value.
                                                                                              
    Cleric:
    Also restores a Cleric's current mana to its maximum value (which is 5 times its magic).
- regenerate
  - No inputs.
  - Increases the fighter's current hit points by an amount equal to one sixth of
    the fighter's strength.  This method must increase the fighter's current hit
    points by at least one.  Do not allow the current hit points to exceed the
    maximum hit points.
                                                                                                     
    Cleric:
    Also increases a Cleric's current mana by an amount equal to one fifth of the
    Cleric's magic.  This method must increase the Cleric's current mana by at
    least one.  Do not allow the current mana to exceed the maximum mana (again, 5 times its magic).
- useAbility
  - No inputs.
  - Attempts to perform a special ability based on the type of fighter.  The
    fighter will attempt to use this special ability just prior to attacking
    every turn.
                                                                                                                                        
    Robot: Shockwave Punch
    Adds bonus damage to the Robot's next attack (and only its next attack) equal to (strength  * ((current_energy/maximum_energy)^4)).
    Can only be used if the Robot has at least [ROBOT_ABILITY_COST] energy.
    Decreases the Robot's current energy by [ROBOT_ABILITY_COST] (after calculating the additional damage) when used.
    	Examples:
    	strength=20, current_energy=20, maximum_energy=20		=> bonus_damage=20
    	strength=20, current_energy=15, maximum_energy=20		=> bonus_damage=6
    	strength=20, current_energy=10, maximum_energy=20		=> bonus_damage=1
    	strength=20, current_energy=5,  maximum_energy=20		=> bonus_damage=0
    Robot Note:
    The bonus damage formula should be computed using double arithmetic, and only
    the final result should be cast into an integer.
                                                                                                                                        
    Archer: Quickstep
    Increases the Archer's speed by one point each time the ability is used.
    This bonus lasts until the reset() method is used.
    This ability always works; there is no maximum bonus speed.
                                                                                                                                        
    Cleric: Healing Light
    Increases the Cleric's current hit points by an amount equal to one third of its magic.
    Can only be used if the Cleric has at least [CLERIC_ABILITY_COST] mana.
    Will be used even if the Cleric's current HP is equal to their maximum HP.
    Decreases the Cleric's current mana by [CLERIC_ABILITY_COST] when used.
    Cleric Note:
    This ability, when successful, must increase the Cleric's current hit points
    by at least one, unless doing so would given the Cleric more hit points than its maximum hit points.
    Do not allow the current hit points to exceed the maximum hit points.
                                                                                                                                        
    Return true if the ability was used; false otherwise.
Note:
  ROBOT_ABILITY_COST = 5;
  CLERIC_ABILITY_COST = 25;

= ARENA =
The arena is a holding aria for fighters which can be accessed by the test
runner.
== ARENA INTERFACE ==
The functions:
- addFighter
  - Given a string of the form:
      <Name> <Class Abbreviation> <Max HP> <Strength> <Speed> <Magic>
    Where Name is a string consisting entirely of alpha-numeric characters and
    the under bar,
    Class Abbreviations is one of
    - A : Archer,
    - C : Cleric, or
    - R : Robot; and
    Max HP, Strength, Speed, and Magic are each positive integer values.
  - Adds a new fighter to the collection of fighters in the arena. Do not allow
    duplicate names.  Reject any string that does not adhere to the format
    outlined above.
  - Return true if a new fighter was added; false otherwise.
- removeFighter
  - Given a string consisting entirely of alpha-numeric characters and the unbar
    bar,
  - Removes the fighter whose name is equal to the given string.  Does nothing if
    no fighter is found with the given name.
  - Return true if a fighter is removed; false otherwise.
- getFighter
  - Given a string consisting entirely of alpha-numeric characters and the unbar
    bar,
  - Returns a reference to the fighter whose name is equal to the given name or
    fails appropriately.
- getSize
  - No inputs.
  - Returns the number of fighters in the arena.
  - Return a non-negative integer.

= DRIVER =
The driver reads commands from the test files under test/input and attempts to
complete each by calling the methods of the arena and fighter interface. It
supports the following case insensitive commands:
- no Ability
  - Valid only as the first command in a file.
  - Indicates that the useAbility and regenerate functions should be called for
    the battles in the given input file. (default)
- use Ability
  - Valid only as the first command in a file.
  - Indicates that the useAbility and regenerate functions should not be called
    for the battles in the given input file.
- addFighter
  - Command to add a fighter to the arena.
  - This command passes it's command line arguments into the addFighter function
    of the arena.
  - Upon success the input command is printed to the output file, followed on
    the same line by the string " Added", otherwise with the string " Invalid"
    appended to the line.
- removeFighter
  - Command removes a fighter from the arena.
  - This command passes it's first argument to the removeFighter function of the
    arena interface.
  - Upon success the input command is printed to the output file, followed on
    the same line by the string " True", otherwise with the string " False"
    appended to the line.
- getFighter
  - Command to inspect the status of a fighter in the arena.
  - Upon success the command is printed to the output file, followed on the next
    line by the information from the fighter in the formatt:
      <Name> <Current HP> <Max HP> <Strength> <Speed> <Magic>
  - Upon failure, the command is printed to the output file, followed on the
    same line by the string " NULL"
- getSize
  - Command reports the number of fighters currently in the arena.
  - Upon success the command is reprinted to the output file followed by a
    single space and the integer number of fighters before the newline.
- reset
  - Command to reset a fighter to it's initial state.
  - Upon success the command is reprinted to the output file, followed on the
    same line by the string " Success" if the fighter was found and reset, or
    " NULL" otherwise.
- battle
  - Command to have two fighters, whose names are given on the same line, enter
    combat.
  - Upon success, the command is reprinted to the output file followed by a log
    of the fight. The log consists of a blank line, and a core statistics line
    for each fighter in the order they where given to the battle command. 
    Detail lines have the following format:
      <Name> <Current HP> <Max HP> <Strength> <Speed> <Magic>
    The combat loop occurs as follows as long as both fighters have positive HP:
    - If abilities are being used:
      - Fighter 1 regenerates and uses ability
    - Fighter 1 is queried for damage dealt
    - Damage is applied to Fighter 2
    - If Fighter 2's HP has dropped below 1 print both fighters details after a
      newline and return.
    - If abilities are being used:
      - Fighter 2 regenerates and uses ability
    - Fighter 2 is queried for damage dealt
    - Damage is applied to Fighter 1
    - Print both fighters details after a newline and continue the loop.
  - If one or both fighters are not found in the arena, the command is printed
    to the output file followed by the string " Fighter not found" before the
    newline.
