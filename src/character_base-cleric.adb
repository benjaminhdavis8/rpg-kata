-------------------------------------------------------------------------------
-- Package Name:
--
--    Character_Base
--
-- Purpose:
--
--    Declare the type from which all character classes for the role playing
--    game will descend.
--
-------------------------------------------------------------------------------

package body Character_Base.Cleric is

   -- Create a Cleric.
   function Initialize
        (Name     : in     Fighter_Name;
         Health   : in     Hit_Points_Type;
         Strength : in     Strength_Type;
         Speed    : in     Speed_Type;
         Magic    : in     Magic_Type) return Cleric_Type is
      Fighter : Cleric_Type;
   begin
      Cleric.Initialize(Fighter  => Fighter,
                        Name     =>  Name,
                        Health   =>  Health,
                        Strength =>  Strength,
                        Speed    =>  Speed,
                        Magic    =>  Magic);
      return Fighter;
   end Initialize;

   function Get_Maximum_Mana
       (Fighter : Cleric_Type'Class)
       return Mana_Type;

   procedure Initialize
        (Fighter  : in out Cleric_Type'Class;
         Name     : in     Fighter_Name;
         Health   : in     Hit_Points_Type;
         Strength : in     Strength_Type;
         Speed    : in     Speed_Type;
         Magic    : in     Magic_Type) is
   begin
      Character_Base.Initialize
           (Fighter    => Fighter,
            Name       => Name,
            Health     => Health,
            Strength   => Strength,
            Speed      => Speed,
            Magic      => Magic);
      Fighter.Cur_Mana := Get_Maximum_Mana(Fighter);
   end Initialize;

   function Get_Damage
        (Fighter : in out Cleric_Type)
        return Damage_Type is
   begin
      return Damage_Type(Get_Magic(Character_Base.Fighter_Type(Fighter)));
   end Get_Damage;

   procedure Reset(Fighter : in out Cleric_Type) is
   begin
      Character_Base.Reset(Fighter => Character_Base.Fighter_Type(Fighter));
      Fighter.Cur_Mana := Get_Maximum_Mana(Fighter);
   end Reset;

   procedure Regenerate
        (Fighter : in out Cleric_Type) is
      Magic_Per_Mana_Regen : constant := 5;
      Mana_Regenerated : Mana_Type;
      Maximum_Mana     : Mana_Type := Get_Maximum_Mana(Fighter);
   begin
      Character_Base.Regenerate(Character_Base.Fighter_Type(Fighter));
      Mana_Regenerated := Mana_Type(Get_Magic(Fighter)) / Magic_Per_Mana_Regen;
      if Mana_Regenerated < 1
      then
         Mana_Regenerated := 1;
      end if;
      Fighter.Cur_Mana := Fighter.Cur_Mana + Mana_Regenerated;
      if Fighter.Cur_Mana > Maximum_Mana then
         Fighter.Cur_Mana := Maximum_Mana;
      end if;
   end Regenerate;

   procedure Use_Ability(Fighter : in out Cleric_Type) is
      Magic_Per_Healing : constant := 3;
      Healing : Hit_Points_Type;
   begin
      if (Fighter.Cur_Mana >= Cleric_Ability_Cost)
      then
         Healing := Hit_Points_Type(Get_Magic(Fighter) / Magic_Per_Healing);
         if Healing < 1
         then
            Healing := 1;
         end if;
         Fighter.Cur_Health := Fighter.Cur_Health + Healing;

         if Fighter.Cur_Health > Fighter.Max_Health
         then
            Fighter.Cur_Health := Fighter.Max_Health;
         end if;
         Fighter.Cur_Mana := Fighter.Cur_Mana - Cleric_Ability_Cost;
      end if;
   end Use_Ability;

   function Get_Maximum_Mana
       (Fighter : Cleric_Type'Class)
       return Mana_Type
   is

      Cleric_Mana_Per_Magic : constant := 5;

   begin
      return Mana_Type(
           Character_Base.Get_Magic(Character_Base.Fighter_Type(Fighter)) *
           Cleric_Mana_Per_Magic);
   end Get_Maximum_Mana;

 end Character_Base.Cleric;
