-------------------------------------------------------------------------------
-- Package Name:
--
--    Character_Base
--
-- Purpose:
--
--    Declare the type from which all character classes for the role playing
--    game will descend.
--
-------------------------------------------------------------------------------

package body Robot is

   -- Constants
   Robot_Energy_Per_Magic : constant := 2;

   -- Create an Robot.
   function Initialize
        (Name     : in     Fighter_Name;
         Health   : in     Hit_Points_Type;
         Strength : in     Strength_Type;
         Speed    : in     Speed_Type;
         Magic    : in     Magic_Type) return Robot_Type is
      Fighter : Robot_Type;
   begin
      Robot.Initialize(Fighter  => Fighter,
                        Name     =>  Name,
                        Health   =>  Health,
                        Strength =>  Strength,
                        Speed    =>  Speed,
                        Magic    =>  Magic);
      return Fighter;
   end Initialize;

   procedure Initialize
        (Fighter  : in out Robot_Type'Class;
         Name     : in     Fighter_Name;
         Health   : in     Hit_Points_Type;
         Strength : in     Strength_Type;
         Speed    : in     Speed_Type;
         Magic    : in     Magic_Type) is
   begin
      Character_Base.Initialize
           (Fighter    => Fighter,
            Name       => Name,
            Health     => Health,
            Strength   => Strength,
            Speed      => Speed,
            Magic      => Magic);
      Fighter.Max_Energy := Energy_Type(
           Character_Base.Get_Magic(Character_Base.Fighter_Type(Fighter)) *
                     Robot_Energy_Per_Magic);
      Fighter.Cur_Energy := Fighter.Max_Energy;
      Fighter.Bonus_Damage := 0;
   end Initialize;

   function Get_Damage
        (Fighter : in out Robot_Type)
        return Damage_Type is
      Damage : Damage_Type;
   begin
      Damage := Damage_Type(
           Bonus_Damage_Type(Get_Strength(Fighter)) + Fighter.Bonus_Damage);
      Fighter.Bonus_Damage := 0;
      return Damage;
   end Get_Damage;

   procedure Reset(Fighter : in out Robot_Type) is
   begin
      Character_Base.Reset(Fighter => Character_Base.Fighter_Type(Fighter));
      Fighter.Cur_Energy := Fighter.Max_Energy;
      Fighter.Bonus_Damage := 0;
   end Reset;

   procedure Use_Ability(Fighter : in out Robot_Type) is
      Ability_Damage : Float;
   begin
      if (Fighter.Cur_Energy >= Robot_Ability_Cost)
      then
         Ability_Damage :=
            Float(Fighter.Cur_Energy) / Float(Fighter.Max_Energy);
         Ability_Damage := Ability_Damage * Ability_Damage;
         Ability_Damage := Ability_Damage * Ability_Damage;
         Ability_Damage := Ability_Damage * Float(Get_Strength(Fighter));
         Fighter.Bonus_Damage := Bonus_Damage_Type(Float'Floor(Ability_Damage));

         Fighter.Cur_Energy := Fighter.Cur_Energy - Robot_Ability_Cost;

      end if;
   end Use_Ability;

 end Robot;
