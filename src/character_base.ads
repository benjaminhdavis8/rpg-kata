-------------------------------------------------------------------------------
-- Package Name:
--
--    Character_Base
--
-- Purpose:
--
--    Declare the type from which all character classes for the role playing
--    game will descend.
--
-------------------------------------------------------------------------------
with Ada.Strings.Maps;

package Character_Base is

   -- Exceptions
   Virtuel_Function : exception;

   -- Constants
   Max_Fighter_Name_Characters : constant Integer := 64;
   Fighter_Name_Chars : constant Ada.Strings.Maps.Character_Set :=
      Ada.Strings.Maps.To_Set("ABCDEFGHIJKLMNOPQRSTUVWXYZ" &
                              "abcdefghijklmnopqrstuvwxyz" &
                              "0123456789" & "_-=+~");

   -- Types
   subtype Fighter_Name is String(1..Max_Fighter_Name_Characters);
   subtype Hit_Points_Type is Integer;
   subtype Starting_Hit_Points_Type is
      Hit_Points_Type range 1 .. Hit_Points_Type'Last;
   subtype Damage_Type is
      Hit_Points_Type range 1 .. Hit_Points_Type'Last;
   type Strength_Type is range 1 .. Positive'Last;
   type Speed_Type is range 1 .. Positive'Last;
   type Magic_Type is range 1 .. Positive'Last;

   type Fighter_Type is tagged private;

   ----------------------------------------------------------------------------
   -- Public constants
   ----------------------------------------------------------------------------
   Null_Fighter : constant Fighter_Type;

   ----------------------------------------------------------------------------
   -- Public subprograms
   ----------------------------------------------------------------------------

   -- Initialize a Fighter.
   procedure Initialize
      (Fighter  : in out Fighter_Type'Class;
       Name     : in     Fighter_Name;
       Health   : in     Hit_Points_Type;
       Strength : in     Strength_Type;
       Speed    : in     Speed_Type;
       Magic    : in     Magic_Type);

   -- Returns a summary of this fighter in the format:
   --   <Name> <Current HP> <Max HP> <Strength> <Speed> <Magic>
   function To_String (Fighter : in Fighter_Type'Class)
        return String;

   -- Returns the name of this fighter.
   function Get_Name (Fighter : in Fighter_Type)
        return String;

   -- Returns the maximum hit points of this fighter.
   function Get_Maximum_Health_Points
        (Fighter : in Fighter_Type)
        return Starting_Hit_Points_Type;

   -- Returns the current hit points of this fighter.
   function Get_Current_Health_Points
        (Fighter : in Fighter_Type)
        return Hit_Points_Type;

   -- Returns the strength stat of this fighter.
   function Get_Strength
        (Fighter : in Fighter_Type)
        return Strength_Type;

   -- Returns the speed stat of this fighter.
   function Get_Speed
        (Fighter : in Fighter_Type)
        return Speed_Type;

   -- Returns the magic stat of this fighter.
   function Get_Magic
        (Fighter : in Fighter_Type)
        return Magic_Type;

   -- Returns the amount of damage a fighter will deal, which depends on it's
   -- class.
   --     Robot:
   --         This value is equal to the Robot's strength plus any additional
   --         damage added for having just used its special ability.
   --     Archer:
   --         This value is equal to the Archer's speed.
   --     Cleric:
   --         This value is equal to the Cleric's magic.
   function Get_Damage
        (Fighter : in out Fighter_Type)
        return Damage_Type;

   -- Reduces the fighter's current hit points by an amount equal to the given
   -- damage minus one fourth of the fighter's speed.  This method must reduce
   -- the fighter's current hit points by at least one.  It is acceptable for
   -- this method to give the fighter negative current hit points.
   --
   -- Examples:
   --     damage=10, speed=7              => damage_taken=9
   --     damage=10, speed=9              => damage_taken=8
   --     damage=10, speed=50             => damage_taken=1
   --
   procedure Take_Damage
        (Fighter : in out Fighter_Type'Class;
         Damage  : in     Damage_Type);

   -- Restores a fighter's current hit points to its maximum hit points.
   --
   --  Robot:
   --  Also restores a Robot's current energy to its maximum value (which is 2
   --  times its magic).  Also resets a Robot's bonus damage (calculated when
   --  a Robot uses its ability) to 0.
   --
   --  Archer:
   --  Also resets an Archer's current speed to its original value.
   --
   --  Cleric: Also restores a Cleric's current mana to its maximum value
   --  (which is 5 times its magic).
   procedure Reset
        (Fighter : in out Fighter_Type);

   -- Increases the fighter's current hit points by an amount equal to one
   -- sixth of the fighter's strength.  This method must increase the
   -- fighter's current hit points by at least one.  Do not allow the current
   -- hit points to exceed the maximum hit points.
   --
   -- Cleric:
   -- Also increases a Cleric's current mana by an amount equal to one fifth
   -- of the Cleric's magic.  This method must increase the Cleric's current
   -- mana by at least one.  Do not allow the current mana to exceed the
   -- maximum mana (again, 5 times its magic).
   procedure Regenerate
        (Fighter : in out Fighter_Type);


   -- Attempts to perform a special ability based on the type of fighter.  The
   -- fighter will attempt to use this special ability just prior to attacking
   -- every turn.
   --
   -- Robot: Shockwave Punch
   -- Adds bonus damage to the Robot's next attack (and only its next attack)
   -- equal to (strength  * ((current_energy/maximum_energy)^4)).  Can only be
   -- used if the Robot has at least [ROBOT_ABILITY_COST] energy.  Decreases
   -- the Robot's current energy by [ROBOT_ABILITY_COST] (after calculating
   -- the additional damage) when used.
   -- 	Examples:
   -- 	strength=20, current_energy=20, maximum_energy=20   => bonus_damage=20
   -- 	strength=20, current_energy=15, maximum_energy=20   => bonus_damage=6
   -- 	strength=20, current_energy=10, maximum_energy=20   => bonus_damage=1
   -- 	strength=20, current_energy=5,  maximum_energy=20   => bonus_damage=0
   -- Robot Note:
   -- The bonus damage formula should be computed using double arithmetic, and
   -- only the final result should be cast into an integer.
   --
   -- Archer: Quickstep
   -- Increases the Archer's speed by one point each time the ability is used.
   -- This bonus lasts until the reset() method is used.
   -- This ability always works; there is no maximum bonus speed.
   --
   -- Cleric: Healing Light
   -- Increases the Cleric's current hit points by an amount equal to one
   -- third of its magic.  Can only be used if the Cleric has at least
   -- [CLERIC_ABILITY_COST] mana.  Will be used even if the Cleric's current
   -- HP is equal to their maximum HP.  Decreases the Cleric's current mana by
   -- [CLERIC_ABILITY_COST] when used.
   -- Cleric Note:
   -- This ability, when successful, must increase the Cleric's current hit
   -- points by at least one, unless doing so would given the Cleric more hit
   -- points than its maximum hit points.  Do not allow the current hit points
   -- to exceed the maximum hit points.
   --
   -- Return true if the ability was used; false otherwise.
   procedure Use_Ability
        (Fighter : in out Fighter_Type);

   -- Return true if the data comprising two fighters is equal.
   function "=" (Left, Right : Fighter_Name) return Boolean;

   -- Return true if the data comprising two fighters is equal.
   function "=" (Left, Right : Fighter_Type'Class) return Boolean;

   private

   type Fighter_Type is
      tagged record
         Name       : Fighter_Name;
         Max_Health : Starting_Hit_Points_Type;
         Cur_Health : Hit_Points_Type;
         Strength   : Strength_Type;
         Speed      : Speed_Type;
         Magic      : Magic_Type;
      end record;

   Null_Fighter : constant Fighter_Type :=
        (Name       => (others => ' '),
         Max_Health => Starting_Hit_Points_Type'First,
         Cur_Health => Hit_Points_Type'First,
         Strength   => Strength_Type'First,
         Speed      => Speed_Type'First,
         Magic      => Magic_Type'First);

end Character_Base;
