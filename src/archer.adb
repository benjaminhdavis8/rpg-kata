-------------------------------------------------------------------------------
-- Package Name:
--
--    Character_Base
--
-- Purpose:
--
--    Declare the type from which all character classes for the role playing
--    game will descend.
--
-------------------------------------------------------------------------------

package body Archer is

   -- Create an Archer.
   function Initialize
        (Name     : in     Fighter_Name;
         Health   : in     Hit_Points_Type;
         Strength : in     Strength_Type;
         Speed    : in     Speed_Type;
         Magic    : in     Magic_Type) return Archer_Type is
      Fighter : Archer_Type;
   begin
      Archer.Initialize(Fighter  => Fighter,
                        Name     =>  Name,
                        Health   =>  Health,
                        Strength =>  Strength,
                        Speed    =>  Speed,
                        Magic    =>  Magic);
      return Fighter;
   end Initialize;

   procedure Initialize
        (Fighter  : in out Archer_Type;
         Name     : in     Fighter_Name;
         Health   : in     Hit_Points_Type;
         Strength : in     Strength_Type;
         Speed    : in     Speed_Type;
         Magic    : in     Magic_Type) is
   begin
      Character_Base.Initialize
           (Fighter    => Fighter,
            Name       => Name,
            Health     => Health,
            Strength   => Strength,
            Speed      => Speed,
            Magic      => Magic);
      Fighter.Effective_Speed := Speed;
   end Initialize;

   function Get_Speed
        (Fighter : in Archer_Type)
        return Speed_Type is
   begin
      return Fighter.Effective_Speed;
   end Get_Speed;

   function Get_Damage
        (Fighter : in out Archer_Type)
        return Damage_Type is
   begin
      return Damage_Type(Archer.Get_Speed(Fighter));
   end Get_Damage;

   procedure Reset(Fighter : in out Archer_Type) is
   begin
      Character_Base.Reset(Fighter => Character_Base.Fighter_Type(Fighter));
      Fighter.Effective_Speed := Character_Base.Get_Speed
           (Fighter => Character_Base.Fighter_Type(Fighter));
   end Reset;

   procedure Use_Ability(Fighter : in out Archer_Type) is
   begin
      Fighter.Effective_Speed := Fighter.Effective_Speed + 1;
   end Use_Ability;

 end Archer;
