-------------------------------------------------------------------------------
-- Function name:
--
--    Drive
--
-- Purpose:
--
--    Interprets instructions from the test files, executes requested
--    functionality and reports the results into a file.
--
-------------------------------------------------------------------------------
with Ada.Command_Line;
with Ada.Directories;
with Ada.Strings.Maps;
with Ada.Strings.Maps.Constants;
with Ada.Strings.Unbounded;
with Ada.Text_IO;
with Ada.Text_IO.Unbounded_IO;
with Arena;
with Character_Base;

procedure Drive is

   ----------------------------------------------------------------------------
   -- Generic instantiation
   ----------------------------------------------------------------------------
   package Integer_IO is new Ada.Text_IO.Integer_IO(Integer);

   ----------------------------------------------------------------------------
   -- Renames
   ----------------------------------------------------------------------------
   package UBString renames Ada.Strings.Unbounded;
   procedure New_Line(Spacing : in Ada.Text_IO.Positive_Count := 1)
        renames Ada.Text_IO.New_Line;
   procedure Put_Line (Item : in String)
        renames Ada.Text_IO.Put_Line;
   procedure Put (Item : in String)
        renames Ada.Text_IO.Put;
   procedure Put (Item : in UBString.Unbounded_String)
        renames Ada.Text_IO.Unbounded_IO.Put;

   ----------------------------------------------------------------------------
   -- Types
   ----------------------------------------------------------------------------

   ----------------------------------------------------------------------------
   -- Shared subprogram data
   ----------------------------------------------------------------------------
   Abilities_On : Boolean := False;

   ----------------------------------------------------------------------------
   -- Subroutines
   ----------------------------------------------------------------------------
   procedure Handle_No_Ability_Command is
   begin
      Abilities_On := False;
   end Handle_No_Ability_Command;
   pragma Inline(Handle_No_Ability_Command);

   procedure Handle_Use_Ability_Command is
   begin
      Abilities_On := True;
   end Handle_Use_Ability_Command;
   pragma Inline(Handle_Use_Ability_Command);

   procedure Handle_Add_Fighter_Command
        (Line : UBString.Unbounded_String) is
      Index1, Index2 : Natural;
      In_Name1 : Character_Base.Fighter_Name := (others => ' ');
   begin
      Ada.Text_IO.Unbounded_IO.Put
           (Item => Line);

      -- Retrieve the bounds of the Fighter description.
      Index1 := UBString.Index_Non_Blank(Source => Line,
                                         Going  => Ada.Strings.Forward);
      Index1 := UBString.Index
           (Source => Line,
            Set    => Ada.Strings.Maps.To_Set(Ada.Strings.Space),
            From   => Index1,
            Going  => Ada.Strings.Forward);
      Index1 := UBString.Index_Non_Blank(Source => Line,
                                         From   => Index1,
                                         Going  => Ada.Strings.Forward);
      Index2 := UBString.Length(Line);
      begin
         Arena.Add_Fighter
              (UBString.Slice(Source => Line,
                              Low    => Index1,
                              High   => Index2));
         Put_Line(Item => " Added");
      exception
         when others =>
            Put_Line(Item => " Invalid");
      end;
   end Handle_Add_Fighter_Command;
   pragma Inline(Handle_Add_Fighter_Command);

   procedure Handle_Remove_Fighter_Command
        (Line : UBString.Unbounded_String) is
      In_Name : Character_Base.Fighter_Name := (others => ' ');
      Index1, Index2 : Natural;
   begin
      Ada.Text_IO.Unbounded_IO.Put
           (Item => Line);

      -- Retrieve the Fighter name.
      Index1 := UBString.Index_Non_Blank(Source => Line,
                                         Going  => Ada.Strings.Forward);
      Index1 := UBString.Index(
           Source => Line,
           Set    => Ada.Strings.Maps.To_Set(Ada.Strings.Space),
           From   => Index1,
           Going  => Ada.Strings.Forward);
      Index1 := UBString.Index_Non_Blank(Source => Line,
                                         From   => Index1,
                                         Going  => Ada.Strings.Forward);
      Index2 :=
         UBString.Index(Source => Line,
                        Set    => Character_Base.Fighter_Name_Chars,
                        From   => Index1,
                        Test   => Ada.Strings.Outside,
                        Going  => Ada.Strings.Forward);
      if Index2 = Natural'First
      then
         Index2 := UBString.Length(Source => Line);
      else
         Index2 := Index2 - 1;
      end if;
      In_Name(In_Name'First .. In_Name'First + (Index2 - Index1)) :=
           UBString.Slice(Source => Line,
                          Low    => Index1,
                          High   => Index2);
      begin
         Arena.Remove_Fighter(In_Name);
         Put_Line(" True");
      exception
         when E : Arena.No_Such_Fighter_Exception =>
            Put_Line(" False");
      end;
   end Handle_Remove_Fighter_Command;
   pragma Inline(Handle_Remove_Fighter_Command);

   procedure Handle_Get_Fighter_Command
        (Line : UBString.Unbounded_String) is
      In_Name : Character_Base.Fighter_Name := (others => ' ');
      Index1, Index2 : Natural;
   begin

      -- Retrieve the Fighter name.
      Index1 := UBString.Index_Non_Blank(Source => Line,
                                         Going  => Ada.Strings.Forward);
      Index1 := UBString.Index(
           Source => Line,
           Set    => Ada.Strings.Maps.To_Set(Ada.Strings.Space),
           From   => Index1,
           Going  => Ada.Strings.Forward);
      Index1 := UBString.Index_Non_Blank(Source => Line,
                                         From   => Index1,
                                         Going  => Ada.Strings.Forward);
      Index2 :=
         UBString.Index(Source => Line,
                        Set    => Character_Base.Fighter_Name_Chars,
                        From   => Index1,
                        Test   => Ada.Strings.Outside,
                        Going  => Ada.Strings.Forward);
      if Index2 = Natural'First
      then
         Index2 := UBString.Length(Line);
      else
         Index2 := Index2 - 1;
      end if;
      In_Name(In_Name'First .. In_Name'First + (Index2 - Index1)) :=
           UBString.Slice(Source => Line,
                          Low    => Index1,
                          High   => Index2);

      begin
         Put(Item => Line);
         declare
            Fighter : Arena.Fighter_Access :=
                 Arena.Get_Fighter(In_Name);
         begin
            New_Line;
            Put_Line("  " & Character_Base.To_String(Fighter.all));
         end;
      exception
         when E : Arena.No_Such_Fighter_Exception =>
            Put_Line(Item => " NULL");
      end;
   end Handle_Get_Fighter_Command;
   pragma Inline(Handle_Get_Fighter_Command);

   procedure Handle_Get_Size_Command
        (Line : UBString.Unbounded_String) is
   begin
      Put(Item => Line);
      Put(Item => " ");
      Integer_IO.Put
           (Item  => Arena.Get_Size,
            Width => 0);
      New_Line;
   end Handle_Get_Size_Command;
   pragma Inline(Handle_Get_Size_Command);

   procedure Handle_Reset_Command
        (Line : UBString.Unbounded_String) is
      In_Name : Character_Base.Fighter_Name := (others => ' ');
      Index1, Index2 : Natural;
   begin

      -- Retrieve the Fighter name.
      Index1 := UBString.Index_Non_Blank(Source => Line,
                                         Going  => Ada.Strings.Forward);
      Index1 := UBString.Index(
           Source => Line,
           Set    => Ada.Strings.Maps.To_Set(Ada.Strings.Space),
           From   => Index1,
           Going  => Ada.Strings.Forward);
      Index1 := UBString.Index_Non_Blank(Source => Line,
                                         From   => Index1,
                                         Going  => Ada.Strings.Forward);
      Index2 :=
         UBString.Index(Source => Line,
                        Set    => Character_Base.Fighter_Name_Chars,
                        From   => Index1,
                        Test   => Ada.Strings.Outside,
                        Going  => Ada.Strings.Forward);
      if Index2 = Natural'First
      then
         Index2 := UBString.Length(Line);
      else
         Index2 := Index2 - 1;
      end if;
      In_Name(In_Name'First .. In_Name'First + (Index2 - Index1)) :=
           UBString.Slice(Source => Line,
                          Low    => Index1,
                          High   => Index2);

      begin
         Put(Item => Line);
         declare
            Fighter : Arena.Fighter_Access :=
                 Arena.Get_Fighter(In_Name);
         begin
            Character_Base.Reset(Fighter => Fighter.all);
            Put_Line(" Success");
         end;
      exception
         when E : Arena.No_Such_Fighter_Exception =>
            Put_Line(Item => " NULL");
      end;
   end Handle_Reset_Command;
   pragma Inline(Handle_Reset_Command);

   procedure Handle_Battle_Command
      (Line : UBString.Unbounded_String) is separate;

   procedure Parse_Command (Line : UBString.Unbounded_String)
   is

      Down_Case_Map : constant Ada.Strings.Maps.Character_Mapping :=
           Ada.Strings.Maps.Constants.Lower_Case_Map;

      Initial_Non_Blank : constant Natural :=
           UBString.Index_Non_Blank(Source => Line,
                                    Going  => Ada.Strings.Forward);

   begin

      -- Skip empty lines
      if UBString.Length(Line) = Natural'First or
         Initial_Non_Blank = Natural'First then
         return;
      end if;

      if UBString.Index(Source  => Line,
                        Pattern => "no ability",
                        From    => Positive'First,
                        Going   => Ada.Strings.Forward,
                        Mapping => Down_Case_Map) =
         Initial_Non_Blank
      then
         Handle_No_Ability_Command;

      elsif UBString.Index(Source    => Line,
                           Pattern   => "use ability",
                           From      => Positive'First,
                           Going   => Ada.Strings.Forward,
                           Mapping   => Down_Case_Map) =
         Initial_Non_Blank
      then
         Handle_Use_Ability_Command;

      elsif UBString.Index(Source    => Line,
                           Pattern   => "addfighter",
                           From      => Positive'First,
                           Going   => Ada.Strings.Forward,
                           Mapping   => Down_Case_Map) =
         Initial_Non_Blank
      then
         Handle_Add_Fighter_Command(Line => Line);

      elsif UBString.Index(Source    => Line,
                           Pattern   => "removefighter",
                           From      => Positive'First,
                           Going   => Ada.Strings.Forward,
                           Mapping   => Down_Case_Map) =
         Initial_Non_Blank
      then
         Handle_Remove_Fighter_Command(Line => Line);

      elsif UBString.Index(Source    => Line,
                           Pattern   => "getfighter",
                           From      => Positive'First,
                           Going   => Ada.Strings.Forward,
                           Mapping   => Down_Case_Map) =
         Initial_Non_Blank
      then
         Handle_Get_Fighter_Command(Line => Line);

      elsif UBString.Index(Source    => Line,
                           Pattern   => "getsize",
                           From      => Positive'First,
                           Going     => Ada.Strings.Forward,
                           Mapping   => Down_Case_Map) =
         Initial_Non_Blank
      then
         Handle_Get_Size_Command(Line => Line);

      elsif UBString.Index(Source    => Line,
                           Pattern   => "reset",
                           From      => Positive'First,
                           Going   => Ada.Strings.Forward,
                           Mapping   => Down_Case_Map) =
         Initial_Non_Blank
      then
         Handle_Reset_Command(Line => Line);

      elsif UBString.Index(Source    => Line,
                           Pattern   => "battle",
                           From      => Positive'First,
                           Going     => Ada.Strings.Forward,
                           Mapping   => Down_Case_Map) =
         Initial_Non_Blank
      then
         Handle_Battle_Command(Line => Line);

      else
         Put_Line("Unknown command");
      end if;

   end Parse_Command;

   ----------------------------------------------------------------------------
   -- Function data
   ----------------------------------------------------------------------------
   Output_File_Directory : String := "./";

begin

   if Ada.Command_Line.Argument_Count < 1
   then
      Put_Line("Usage: " & Ada.Command_Line.Command_Name & " <input file>");
      New_Line;
      return;
   end if;

   Put_Line("Processing input from '" &
            Ada.Command_Line.Argument(1) & "'");

   Command_File_Processing :
   declare
      Initial_Input_Line_Chars : constant Positive := 80;

      Input_File_Path  : String :=
           Ada.Directories.Full_Name(Ada.Command_Line.Argument(1));
      Output_File_Path : String :=
           Ada.Directories.Compose
                (Containing_Directory =>
                      Ada.Directories.Full_Name(Output_File_Directory),
                 Name                 => "out_" &
                      Ada.Directories.Base_Name(Ada.Command_Line.Argument(1)),
                 Extension            => "txt");

      Output_File      : Ada.Text_IO.File_Type;
      Input_File       : Ada.Text_IO.File_Type;
      Input_Line       : UBString.Unbounded_String :=
           UBString.To_Unbounded_String(Length => Initial_Input_Line_Chars);

   begin
      Put_Line("Saving output to '" & Output_File_Path & "'");

      Ada.Text_IO.Open
           (File => Input_File,
            Mode => Ada.Text_IO.In_File,
            Name => Input_File_Path);
      Ada.Text_IO.Create
           (File => Output_File,
            Mode => Ada.Text_IO.Out_File,
            Name => Output_File_Path);

      Ada.Text_IO.Set_Output(File => Output_File);
      while not Ada.Text_IO.End_Of_File(Input_File)
      loop
         Ada.Text_IO.Unbounded_IO.Get_Line
              (File => Input_File,
               Item => Input_Line);

         Parse_Command (Input_Line);
      end loop;
      Ada.Text_IO.Set_Output(File => Ada.Text_IO.Standard_Output);
      Ada.Text_IO.Close(File => Input_File);
      Ada.Text_IO.Close(File => Output_File);

   end Command_File_Processing;
   Put_Line("Processing completed");

end Drive;
