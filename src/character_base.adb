-------------------------------------------------------------------------------
-- Package Name:
--
--    Character_Base
--
-- Purpose:
--
--    Declare the type from which all character classes for the role playing
--    game will descend.
--
-------------------------------------------------------------------------------
with Ada.Strings.Fixed;
with Ada.Unchecked_Conversion;

package body Character_Base is

   procedure Initialize
        (Fighter  : in out Fighter_Type'Class;
         Name     : in     Fighter_Name;
         Health   : in     Hit_Points_Type;
         Strength : in     Strength_Type;
         Speed    : in     Speed_Type;
         Magic    : in     Magic_Type) is
   begin
      Fighter.Name       := Name;
      Fighter.Max_Health := Health;
      Fighter.Cur_Health := Health;
      Fighter.Strength   := Strength;
      Fighter.Speed      := Speed;
      Fighter.Magic      := Magic;
   end Initialize;

   --   <Name> <Current HP> <Max HP> <Strength> <Speed> <Magic>
   function To_String (Fighter : in Fighter_Type'Class)
        return String is
        function Trim(Source : in String;
                      Side   : in Ada.Strings.Trim_End) return String
             renames Ada.Strings.Fixed.Trim;
   begin
      return Trim(Source => Get_Name(Fighter),
                  Side   => Ada.Strings.Both) & " "
           & Trim(Source => Hit_Points_Type'Image(
                                 Get_Current_Health_Points(Fighter)),
                  Side   => Ada.Strings.Both) & " "
           & Trim(Source => Hit_Points_Type'Image(
                                 Get_Maximum_Health_Points(Fighter)),
                  Side   => Ada.Strings.Both) & " "
           & Trim(Source => Strength_Type'Image(Get_Strength(Fighter)),
                  Side   => Ada.Strings.Both) & " "
           & Trim(Source => Speed_Type'Image(Get_Speed(Fighter)),
                  Side   => Ada.Strings.Both) & " "
           & Trim(Source => Magic_Type'Image(Get_Magic(Fighter)),
                  Side   => Ada.Strings.Both);
   end To_String;

   function Get_Name
        (Fighter : in Fighter_Type)
        return String is
   begin
      return Fighter.Name;
   end Get_Name;

   function Get_Maximum_Health_Points
        (Fighter : in Fighter_Type)
        return Starting_Hit_Points_Type is
   begin
      return Fighter.Max_Health;
   end Get_Maximum_Health_Points;

   function Get_Current_Health_Points
        (Fighter : in Fighter_Type)
        return Hit_Points_Type is
   begin
      return Fighter.Cur_Health;
   end Get_Current_Health_Points;

   function Get_Strength
        (Fighter : in Fighter_Type)
        return Strength_Type is
   begin
      return Fighter.Strength;
   end Get_Strength;

   function Get_Speed
        (Fighter : in Fighter_Type)
        return Speed_Type is
   begin
      return Fighter.Speed;
   end Get_Speed;

   function Get_Magic
        (Fighter : in Fighter_Type)
        return Magic_Type is
   begin
      return Fighter.Magic;
   end Get_Magic;

   function Get_Damage
        (Fighter : in out Fighter_Type)
        return Damage_Type is
   begin
      raise Virtuel_Function with
           "Character_Base.Get_Damage called";
      return 1;
   end Get_Damage;

   procedure Take_Damage
        (Fighter : in out Fighter_Type'Class;
         Damage  : in     Damage_Type) is

       Speed_Per_Mitigation : constant := 4;
       Mitigation           : Hit_Points_Type;

   begin
      Mitigation := Integer (Get_Speed(Fighter) / Speed_Per_Mitigation);
      if Damage > Mitigation
      then
         Fighter.Cur_Health :=
            Fighter.Cur_Health - (Damage - Mitigation);
      else
         Fighter.Cur_Health := Fighter.Cur_Health - 1;
      end if;
   end Take_Damage;

   procedure Reset
        (Fighter : in out Fighter_Type) is
   begin
      Fighter.Cur_Health := Fighter.Max_Health;
   end Reset;

   procedure Regenerate
        (Fighter : in out Fighter_Type) is

      Healing_Per_Strength : constant := 6;
      Healing              : Hit_Points_Type;

   begin
      Healing := Integer (Fighter.Strength / Healing_Per_Strength);

      if Healing > 0
      then
         Fighter.Cur_Health := Fighter.Cur_Health + Healing;
      else
         Fighter.Cur_Health := Fighter.Cur_Health + 1;
      end if;

      if Fighter.Cur_Health > Fighter.Max_Health
      then
         Fighter.Cur_Health := Fighter.Max_Health;
      end if;

   end Regenerate;

   procedure Use_Ability
        (Fighter : in out Fighter_Type) is
   begin
      raise Virtuel_Function with
           "Character_Base.Use_Ability called";
   end Use_Ability;

   function "=" (Left, Right : Character_Base.Fighter_Type'Class)
        return Boolean is
   begin
      return
           Character_Base."="(Left.Name, Right.Name) and
           Left.Max_Health = Right.Max_Health and
           Left.Strength   = Right.Strength and
           Left.Speed      = Right.Speed and
           Left.Magic      = Right.Magic;
   end;

   function "=" (Left, Right : Fighter_Name) return Boolean is

      Trimed_Left : String :=
         Ada.Strings.Fixed.Trim(Source => Left,
                                Side   => Ada.Strings.Both);
      Trimed_Right : String :=
         Ada.Strings.Fixed.Trim(Source => Right,
                                Side   => Ada.Strings.Both);
      Equal : Boolean := True;
   begin
      if ((Trimed_Left'Last - Trimed_Left'First) =
          (Trimed_Right'Last - Trimed_Right'First)) then
         for Index in 0 .. (Trimed_Left'Last - Trimed_Left'First)
         loop
           if Trimed_Left(Trimed_Left'First + index) /=
              Trimed_Right(Trimed_Right'First + index)
           then
              Equal := False;
              exit;
           end if;
         end loop;
      else
         Equal := False;
      end if;
      return Equal;
   end "=";

end Character_Base;
