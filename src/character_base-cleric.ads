-------------------------------------------------------------------------------
--
--    Declare the type of cleric class characters for the role playing
--    game, and implement cleric unique processing.
--
-- Constraints:
--
--    The cleric ability requires that the package is able to add to the
--    fighters current health. This could have been done with a routine in
--    Character_Base, but was accomplished using child package visibility.
--
-------------------------------------------------------------------------------
with Character_Base;
use  Character_Base;

package Character_Base.Cleric is

   -- Types
   type Cleric_Type is new Fighter_Type with private;
   type Mana_Type is range 0 .. Positive'Last;

   -- Constants
   Cleric_Ability_Cost : constant Mana_Type := 25;

   ----------------------------------------------------------------------------
   -- Public subprograms
   ----------------------------------------------------------------------------

   -- Create a cleric.
   function Initialize
      (Name     : in     Fighter_Name;
       Health   : in     Hit_Points_Type;
       Strength : in     Strength_Type;
       Speed    : in     Speed_Type;
       Magic    : in     Magic_Type) return Cleric_Type;

   -- Initialize a cleric.
   procedure Initialize
      (Fighter  : in out Cleric_Type'Class;
       Name     : in     Fighter_Name;
       Health   : in     Hit_Points_Type;
       Strength : in     Strength_Type;
       Speed    : in     Speed_Type;
       Magic    : in     Magic_Type);

   -- Returns the amount of damage the cleric will deal, which  is equal to
   -- the cleric's magic.
   function Get_Damage
      (Fighter : in out Cleric_Type)
      return Damage_Type;

   -- Restores the cleric's current hit points to its maximum hit points, and
   -- resets it's current mana to its maximum value (which is 5 times its magic).
   procedure Reset (Fighter : in out Cleric_Type);

   -- Increases the fighter's current hit points by an amount equal to one
   -- sixth of the fighter's strength.  This method must increase the
   -- fighter's current hit points by at least one.  Do not allow the current
   -- hit points to exceed the maximum hit points.
   --
   -- Cleric:
   -- Also increases a Cleric's current mana by an amount equal to one fifth
   -- of the Cleric's magic.  This method must increase the Cleric's current
   -- mana by at least one.  Do not allow the current mana to exceed the
   -- maximum mana (again, 5 times its magic).
   procedure Regenerate
        (Fighter : in out Cleric_Type);

   -- Attempt to perform the cleric's Healing ability.
   -- Healing increases the cleric's current hit points by an amount equal to
   -- one third of its magic.
   -- Can only be used if the Cleric has at least [Cleric_Ability_Cost] mana.
   -- Will be used even if the Cleric's current HP is equal to their maximum HP.
   -- Decreases the Cleric's current mana by [Cleric_Ability_Cost] when used.
   -- Cleric Note:
   -- This ability, when successful, must increase the Cleric's current hit
   -- points by at least one, unless doing so would given the Cleric more hit
   -- points than its maximum hit points.  Do not allow the current hit points
   -- to exceed the maximum hit points.
   procedure Use_Ability(Fighter : in out Cleric_Type);

private

   type Cleric_Type is new Fighter_Type with
        record
          Cur_Mana : Mana_Type;
          Max_Mana : Mana_Type;
        end record;

 end Character_Base.Cleric;
