-------------------------------------------------------------------------------
-- Package Name:
--
--    Robot
--
-- Purpose:
--
--    Declare the type of Robot class characters for the role playing
--    game, and implement Robot unique processing.
--
-------------------------------------------------------------------------------
with Character_Base;
use  Character_Base;

package Robot is

   -- Types
   type Robot_Type is new Fighter_Type with private;
   type Energy_Type is range 0 .. Positive'Last;
   type Bonus_Damage_Type is new Character_Base.Damage_Type'Base range
        0 .. Character_Base.Damage_Type'Last;

   -- Constants
   Robot_Ability_Cost : constant Energy_Type := 5;

   ----------------------------------------------------------------------------
   -- Public subprograms
   ----------------------------------------------------------------------------

   -- Create an Robot.
   function Initialize
      (Name     : in     Fighter_Name;
       Health   : in     Hit_Points_Type;
       Strength : in     Strength_Type;
       Speed    : in     Speed_Type;
       Magic    : in     Magic_Type) return Robot_Type;

   -- Initialize an Robot.
   procedure Initialize
      (Fighter  : in out Robot_Type'Class;
       Name     : in     Fighter_Name;
       Health   : in     Hit_Points_Type;
       Strength : in     Strength_Type;
       Speed    : in     Speed_Type;
       Magic    : in     Magic_Type);

   -- Returns the amount of damage the Robot will deal, which  is equal to
   -- the Robot's speed.
   function Get_Damage
      (Fighter : in out Robot_Type)
      return Damage_Type;

   -- Restores the Robot's current hit points to its maximum hit points, and
   -- resets it's current speed to its original value.
   procedure Reset (Fighter : in out Robot_Type);

   -- Attempt to perform the Robot's Shockwave Punch ability.
   -- Adds bonus damage to the Robot's next attack (and only its next attack)
   -- equal to (strength  * ((current_energy/maximum_energy)^4)).  Can only be
   -- used if the Robot has at least [ROBOT_ABILITY_COST] energy.  Decreases
   -- the Robot's current energy by [ROBOT_ABILITY_COST] (after calculating
   -- the additional damage) when used.  Examples:
   --    strength=20, current_energy=20, maximum_energy=20   => bonus_damage=20
   --    strength=20, current_energy=15, maximum_energy=20   => bonus_damage=6
   --    strength=20, current_energy=10, maximum_energy=20   => bonus_damage=1
   --    strength=20, current_energy=5,  maximum_energy=20   => bonus_damage=0
   procedure Use_Ability(Fighter : in out Robot_Type);

private

   type Robot_Type is new Fighter_Type with
        record
          Cur_Energy   : Energy_Type;
          Max_Energy   : Energy_Type;
          Bonus_Damage : Bonus_Damage_Type;
        end record;

 end Robot;
