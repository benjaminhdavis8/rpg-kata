with Character_Base; use  Character_Base;
with Archer; use  Archer;
with Character_Base.Cleric; use Character_Base.Cleric;
with Robot; use Robot;

separate (Drive)
procedure Handle_Battle_Command
     (Line : UBString.Unbounded_String) is
   Index1, Index2 : Natural;
   In_Name1, In_Name2 : Fighter_Name := (others => ' ');
begin

   -- Retrieve the first fighter name.
   Index1 := UBString.Index_Non_Blank
        (Source => Line,
         Going  => Ada.Strings.Forward);
   Index1 := UBString.Index
        (Source => Line,
         Set    => Ada.Strings.Maps.To_Set(Ada.Strings.Space),
         From   => Index1,
         Going  => Ada.Strings.Forward);
   Index1 := UBString.Index_Non_Blank
        (Source => Line,
         From   => Index1,
         Going  => Ada.Strings.Forward);
   Index2 := UBString.Index
        (Source => Line,
         Set    => Fighter_Name_Chars,
         From   => Index1,
         Test   => Ada.Strings.Outside,
         Going  => Ada.Strings.Forward);
   if Index2 = Natural'First
   then
      Index2 := UBString.Length(Line);
   else
      Index2 := Index2 - 1;
   end if;
   In_Name1(In_Name1'First .. In_Name1'First + (Index2 - Index1)) :=
        UBString.Slice(Source => Line,
                       Low    => Index1,
                       High   => Index2);

   -- Retrieve the second fighter name.
   Index1 := UBString.Index
        (Source => Line,
         Set    => Ada.Strings.Maps.To_Set(Ada.Strings.Space),
         From   => Index2,
         Going  => Ada.Strings.Forward);
   Index1 := UBString.Index_Non_Blank
        (Source => Line,
         From   => Index1,
         Going  => Ada.Strings.Forward);
   Index2 := UBString.Index
        (Source => Line,
         Set    => Fighter_Name_Chars,
         From   => Index1,
         Test   => Ada.Strings.Outside,
         Going  => Ada.Strings.Forward);
   if Index2 = Natural'First
   then
      Index2 := UBString.Length(Line);
   else
      Index2 := Index2 - 1;
   end if;
   In_Name2(In_Name2'First .. In_Name2'First + (Index2 - Index1)) :=
        UBString.Slice(Source => Line,
                       Low    => Index1,
                       High   => Index2);
   begin
      Put(Item => Line);
      New_Line;
      declare
         Fighter_ptr1 : Arena.Fighter_Access :=
              Arena.Get_Fighter(In_Name1);
         Fighter1 : Character_Base.Fighter_Type'Class renames
              Fighter_ptr1.all;
         Fighter_ptr2 : Arena.Fighter_Access :=
              Arena.Get_Fighter(In_Name2);
         Fighter2 : Character_Base.Fighter_Type'Class renames
              Fighter_ptr2.all;
         Damage   : Damage_Type;
      begin
         New_Line;
         Put_Line("  " & To_String(Fighter1));
         Put_Line("  " & To_String(Fighter2));
         while Get_Current_Health_Points(Fighter1) > 0 and
               Get_Current_Health_Points(Fighter2) > 0
         loop

            if Abilities_On
            then
               Regenerate(Fighter1);
               Use_Ability(Fighter1);
            end if;
            Damage := Get_Damage(Fighter1);
            Take_Damage(Fighter2, Damage);

            if Get_Current_Health_Points(Fighter2) < 1
            then
               New_Line;
               Put_Line("  " & To_String(Fighter1));
               Put_Line("  " & To_String(Fighter2));
               exit;
            end if;

            if Abilities_On
            then
               Regenerate(Fighter2);
               Use_Ability(Fighter2);
            end if;
            Damage := Get_Damage(Fighter2);
            Take_Damage(Fighter1, Damage);

            New_Line;
            Put_Line("  " & To_String(Fighter1));
            Put_Line("  " & To_String(Fighter2));

         end loop;
      end;

   exception
      when E : Arena.No_Such_Fighter_Exception =>
         Put_Line(Item => " Fighter not found");
   end;
end Handle_Battle_Command;
