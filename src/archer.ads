-------------------------------------------------------------------------------
-- Package Name:
--
--    Archer
--
-- Purpose:
--
--    Declare the type of archer class characters for the role playing
--    game, and implement archer unique processing.
--
-------------------------------------------------------------------------------
with Character_Base;
use  Character_Base;

package Archer is

   -- Types
   type Archer_Type is new Fighter_Type with private;

   ----------------------------------------------------------------------------
   -- Public subprograms
   ----------------------------------------------------------------------------

   -- Create an Archer.
   function Initialize
      (Name     : in     Fighter_Name;
       Health   : in     Hit_Points_Type;
       Strength : in     Strength_Type;
       Speed    : in     Speed_Type;
       Magic    : in     Magic_Type) return Archer_Type;

   -- Initialize an Archer.
   procedure Initialize
      (Fighter  : in out Archer_Type;
       Name     : in     Fighter_Name;
       Health   : in     Hit_Points_Type;
       Strength : in     Strength_Type;
       Speed    : in     Speed_Type;
       Magic    : in     Magic_Type);

   -- Returns the archers effective speed.
   function Get_Speed
        (Fighter : in Archer_Type)
        return Speed_Type;

   -- Returns the amount of damage the archer will deal, which  is equal to
   -- the Archer's speed.
   function Get_Damage
      (Fighter : in out Archer_Type)
      return Damage_Type;

   -- Restores the archer's current hit points to its maximum hit points, and
   -- resets it's current speed to its original value.
   procedure Reset (Fighter : in out Archer_Type);

   -- Attempt to perform the archer's Quickstep ability.
   -- Quickstep increases the Archer's speed by one point each time the
   -- ability is used.  This bonus lasts until the reset() method is used.
   -- This ability always works; there is no maximum bonus speed.
   procedure Use_Ability(Fighter : in out Archer_Type);

private

   type Archer_Type is new Fighter_Type with
        record
          Effective_Speed : Speed_Type;
        end record;

 end Archer;
