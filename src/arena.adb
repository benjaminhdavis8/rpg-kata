-------------------------------------------------------------------------------
-- Package name:
--
--    Arena
--
-- Purpose:
--
--    Maintains the list of fighters that have been created.
--
-------------------------------------------------------------------------------
with Ada.Containers.Vectors;
with Ada.Strings.Fixed;
with Ada.Strings.Maps;
with Ada.Text_IO;
with System.Storage_Pools;
with Archer;
with Character_Base;
with Character_Base.Cleric;
with Robot;

package body Arena is

   ----------------------------------------------------------------------------
   -- Constants
   ----------------------------------------------------------------------------
   One_K_Block : constant := 1058;

   ----------------------------------------------------------------------------
   -- Types
   ----------------------------------------------------------------------------
   type Archer_Access is access all Archer.Archer_Type;
   for Archer_Access'Storage_Pool use Fighter_Access'Storage_Pool;
   type Cleric_Access is access all Character_Base.Cleric.Cleric_Type;
   for Cleric_Access'Storage_Pool use Fighter_Access'Storage_Pool;
   type Robot_Access is access all Robot.Robot_Type;
   for Robot_Access'Storage_Pool use Fighter_Access'Storage_Pool;

   ----------------------------------------------------------------------------
   -- Generic instantiations
   ----------------------------------------------------------------------------

   -- Vector type to keep track of allocated fighters.
   function "=" (Left, Right : Fighter_Access)
        return Boolean;
   Initial_Fighter_List_Size : constant := 0;
   type Arena_Index is range Natural'First + 1 .. Natural'Last;
   package Fighter_Containers is new Ada.Containers.Vectors
        (Index_Type   => Arena_Index,
         Element_Type => Fighter_Access,
         "="          => "=");

   package Health_IO is
        new Ada.Text_IO.Integer_IO(Character_Base.Hit_Points_Type);
   package Strength_IO is
        new Ada.Text_IO.Integer_IO(Character_Base.Strength_Type);
   package Speed_IO is
        new Ada.Text_IO.Integer_IO(Character_Base.Speed_Type);
   package Magic_IO is
        new Ada.Text_IO.Integer_IO(Character_Base.Magic_Type);

   ----------------------------------------------------------------------------
   -- Forward declared internal package subprograms
   ----------------------------------------------------------------------------
   function Find_Fighter (Name : Character_Base.Fighter_Name)
        return Fighter_Containers.Extended_Index;

   ----------------------------------------------------------------------------
   -- Package data
   ----------------------------------------------------------------------------
   Instance : aliased Fighter_Containers.Vector :=
      Fighter_Containers.To_Vector(Initial_Fighter_List_Size);

   ----------------------------------------------------------------------------
   -- Public subprograms
   ----------------------------------------------------------------------------
   procedure Add_Fighter (Line : String) is

      -------------------------------------------------------------------------
      -- Renames
      -------------------------------------------------------------------------
      function Index_Non_Blank
           (Source : in String;
            From   : in Positive;
            Going  : in Ada.Strings.Direction) return Natural
           renames Ada.Strings.Fixed.Index_Non_Blank;
      function Index
           (Source : in String;
            Set    : in Ada.Strings.Maps.Character_Set;
            From   : in Positive;
            Test   : in Ada.Strings.Membership;
            Going  : in Ada.Strings.Direction) return Natural
           renames Ada.Strings.Fixed.Index;
      function "="(Left, Right : Fighter_Containers.Cursor) return Boolean
           renames Fighter_Containers."=";

      -------------------------------------------------------------------------
      -- Constants
      -------------------------------------------------------------------------
      Fighter_Class_ID_Chars : constant Ada.Strings.Maps.Character_Set :=
         Ada.Strings.Maps.To_Set("ARC");

      -------------------------------------------------------------------------
      -- Variables
      -------------------------------------------------------------------------
      Index1, Index2 : Natural;
      In_Name : Character_Base.Fighter_Name := (others => ' ');
      In_Class_ID : Character;
      In_Health : Character_Base.Hit_Points_Type;
      In_Strength : Character_Base.Strength_Type;
      In_Speed : Character_Base.Speed_Type;
      In_Magic : Character_Base.Magic_Type;
   begin

      -- Retrieve the Name of the Fighter.
      Index1 := Index_Non_Blank(Source => Line,
                                From   => Line'First,
                                Going  => Ada.Strings.Forward);
      Index2 := Index(Source => Line,
                      Set    => Character_Base.Fighter_Name_Chars,
                      From   => Index1,
                      Test   => Ada.Strings.Outside,
                      Going  => Ada.Strings.Forward);
      if Index2 = Natural'First
      then
         Index2 := Line'Last;
      else
         Index2 := Index2 - 1;
      end if;
      In_Name(In_Name'First .. In_Name'First + (Index2 - Index1)) :=
         Line(Index1 .. Index2);
      if Find_Fighter(In_Name) /= Fighter_Containers.Extended_Index'First
      then
         raise Duplicate_Fighter_Exception with
              "Fighter with name '" & In_Name & "' already exists.";
      end if;

      -- Retrieve the Fighter class identifier.
      Index1 :=
         Index_Non_Blank(Source => Line,
                         From   => Index2 + 1,
                         Going  => Ada.Strings.Forward);
      Index2 :=
         Index(Source => Line,
               Set    => Fighter_Class_ID_Chars,
               From   => Index1,
               Test   => Ada.Strings.Outside,
               Going  => Ada.Strings.Forward);
      if Index1 - Index2 > 1 then
         raise Invalad_Format_Exception with
              "Unknown Class ID: '" & Line(Index1 .. Index2) & "'.";
      end if;
      In_Class_ID := Line(Index1);

      -- Retrieve the Fighter maximum hit points.
      Index1 :=
         Index_Non_Blank(Source => Line,
                         From   => Index2,
                         Going  => Ada.Strings.Forward);
      begin
         Health_IO.Get(
            From => Line(Index1 .. Line'Last),
            Item => In_Health,
            Last => Index2);
      exception
         when others =>
            raise Invalad_Format_Exception with
                 "Bad value for health in '" & Line(Index1 .. Line'Last)
                 & "'.";
      end;

      -- Retrieve the Fighter base strength.
      Index1 := Index2 + 1;
      begin
         Strength_IO.Get(
            From => Line(Index1 .. Line'Last),
            Item => In_Strength,
            Last => Index2);
      exception
         when others =>
            raise Invalad_Format_Exception with
                 "Bad value for strength in '" & Line(Index1 .. Line'Last)
                 & "'.";
      end;

      -- Retrieve the Fighter base speed.
      Index1 := Index2 + 1;
      begin
         Speed_IO.Get(
            From => Line(Index1 .. Line'Last),
            Item => In_Speed,
            Last => Index2);
      exception
         when others =>
            raise Invalad_Format_Exception with
                 "Bad value for speed in '" & Line(Index1 .. Line'Last) & "'";
      end;

      -- Retrieve the Fighter base magic.
      Index1 := Index2 + 1;
      begin
         Magic_IO.Get(
            From => Line(Index1 .. Line'Last),
            Item => In_Magic,
            Last => Index2);
      exception
         when others =>
            raise Invalad_Format_Exception with
                 "Bad value for magic in '" & Line(Index1 .. Line'Last) & "'";
      end;

      --- Ensure there is no trailing characters.
      Index1 := Index2 + 1;
      Index2 :=
           Index_Non_Blank(Source => Line,
                           From   => Index1,
                           Going  => Ada.Strings.Forward);
      if Index2 /= Natural'First
      then
         raise Invalad_Format_Exception with
              "Trailing Characters '" & Line(Index2 .. Line'Last) & "'";
      end if;

      case In_Class_ID is
         when 'A' =>
            declare
               New_Archer : Archer_Access :=
                    new Archer.Archer_Type;
               New_Fighter : Fighter_Access;
               for New_Fighter'address use New_Archer'address;
               pragma Import(Convention    => Ada,
                             Entity        => New_Fighter,
                             External_Name => "Arena_New_Fighter");
            begin
               Archer.Initialize (Fighter  => New_Archer.all,
                                  Name     => In_Name,
                                  Health   => In_Health,
                                  Strength => In_Strength,
                                  Speed    => In_Speed,
                                  Magic    => In_Magic);
               Fighter_Containers.Append(Container => Instance,
                                         New_Item  => New_Fighter);
            end;
         when 'C' =>
            declare
               New_Cleric : Cleric_Access :=
                    new Character_Base.Cleric.Cleric_Type;
               New_Fighter : Fighter_Access;
               for New_Fighter'address use New_Cleric'address;
               pragma Import(Convention    => Ada,
                             Entity        => New_Fighter,
                             External_Name => "Arena_New_Fighter");
            begin
               Character_Base.Cleric.Initialize (Fighter  => New_Cleric.all,
                                                 Name     => In_Name,
                                                 Health   => In_Health,
                                                 Strength => In_Strength,
                                                 Speed    => In_Speed,
                                                 Magic    => In_Magic);
               Fighter_Containers.Append(Container => Instance,
                                         New_Item  => New_Fighter);
            end;
         when 'R' =>
            declare
               New_Robot : Robot_Access :=
                    new Robot.Robot_Type;
               New_Fighter : Fighter_Access;
               for New_Fighter'address use New_Robot'address;
               pragma Import(Convention    => Ada,
                             Entity        => New_Fighter,
                             External_Name => "Arena_New_Fighter");
            begin
               Robot.Initialize (Fighter  => New_Robot.all,
                                 Name     => In_Name,
                                 Health   => In_Health,
                                 Strength => In_Strength,
                                 Speed    => In_Speed,
                                 Magic    => In_Magic);
               Fighter_Containers.Append(Container => Instance,
                                         New_Item  => New_Fighter);
            end;
         when others =>
            raise Invalad_Format_Exception with "Class ID: '" & In_Class_ID &
            "' does not match any class";
      end case;

   end Add_Fighter;

   procedure Remove_Fighter (Name : Character_Base.Fighter_Name) is
      Fighter_Index : Fighter_Containers.Extended_Index :=
         Find_Fighter(Name);

      function "="(Left, Right : Fighter_Containers.Cursor) return Boolean
           renames Fighter_Containers."=";

   begin

      if Fighter_Index = Fighter_Containers.Extended_Index'First
      then
         raise No_Such_Fighter_Exception with "Fighter Name: " & Name;
      end if;

      Fighter_Containers.Delete
           (Container => Instance,
            Index     => Fighter_Index);
   end Remove_Fighter;

   -- Return a reference to the fighter with name Name.
   function Get_Fighter(Name : Character_Base.Fighter_Name)
        return Fighter_Access is

      function "="(Left, Right : Fighter_Containers.Cursor) return Boolean
           renames Fighter_Containers."=";

      Fighter_Index     : Fighter_Containers.Extended_Index :=
         Find_Fighter(Name);
   begin

      if Fighter_Index = Fighter_Containers.Extended_Index'First
      then
         raise No_Such_Fighter_Exception with "Fighter Name: " & Name;
      end if;

      return Fighter_Containers.Element(Instance, Fighter_Index);

   end Get_Fighter;

   function Get_Size return Natural is
   begin
      return Natural(Fighter_Containers.Last_Index(Instance));
   end Get_Size;

   -- Get a cursor to the fighter who's name matches Name.
   function Find_Fighter (Name : Character_Base.Fighter_Name)
        return Fighter_Containers.Extended_Index is

      Index : Fighter_Containers.Extended_Index :=
           Fighter_Containers.First_Index(Instance);
      Found : Boolean := False;
   begin

      while Index <= Fighter_Containers.Last_Index(Instance)
      loop
         if Name =
            Character_Base.Get_Name
                 (Fighter_Containers.Reference(Instance, Index).all)
         then
            Found := True;
            exit;
         end if;
         Index := Fighter_Containers.Extended_Index'Succ(Index);
      end loop;

      if not Found
      then
         Index := Fighter_Containers.Extended_Index'First;
      end if;
      return Index;

   end Find_Fighter;

   -- Comparison subprogram to pass through to base type comparison function.
   function "=" (Left, Right : Fighter_Access) return Boolean is
      function "=" (Left, Right : Character_Base.Fighter_Type) return Boolean
           renames Character_Base."=";
   begin
      return Left.all = Right.all;
   end "=";


end Arena;
