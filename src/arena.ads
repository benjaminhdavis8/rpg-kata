-------------------------------------------------------------------------------
-- Package name:
--
--    Arena
--
-- Purpose:
--
--    Maintains the list of fighters that have been created.
--
-------------------------------------------------------------------------------
with Character_Base;

package Arena is

   ----------------------------------------------------------------------------
   -- Types
   ----------------------------------------------------------------------------
   type Fighter_Access is access all Character_Base.Fighter_Type'Class;

   ----------------------------------------------------------------------------
   -- Adds a new fighter to the collection of fighters in the arena. Do not
   -- allow duplicate names.  Reject any string that does not adhere to the
   -- format:
   --
   --       <Name> <Class Abbreviation> <Max HP> <Strength> <Speed> <Magic>
   --
   ----------------------------------------------------------------------------
   procedure Add_Fighter (Line : String);

   ----------------------------------------------------------------------------
   -- Given a string consisting entirely of alpha-numeric characters and the
   -- unbar bar,
   -- Removes the fighter whose name is equal to the given string.  Does
   -- nothing if no fighter is found with the given name.
   -- Return true if a fighter is removed; false otherwise.
   ----------------------------------------------------------------------------
   procedure Remove_Fighter (Name : Character_Base.Fighter_Name);

   ----------------------------------------------------------------------------
   -- Given a string consisting entirely of alpha-numeric characters and the
   -- unbar bar,
   -- Returns a reference to the fighter whose name is equal to the given name
   -- or fails appropriately.
   ----------------------------------------------------------------------------
   function Get_Fighter (Name : Character_Base.Fighter_Name)
      return Fighter_Access;

   ----------------------------------------------------------------------------
   -- Returns the number of fighters in the arena.
   ----------------------------------------------------------------------------
   function Get_Size return Natural;

   ----------------------------------------------------------------------------
   -- Exceptions
   ----------------------------------------------------------------------------
   Duplicate_Fighter_Exception : exception;
   Invalad_Format_Exception    : exception;
   No_Such_Fighter_Exception   : exception;

end Arena;
